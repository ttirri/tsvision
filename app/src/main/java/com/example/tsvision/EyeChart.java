package com.example.tsvision;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Random;

public class EyeChart implements VisionChart {
    private final Context mContext;
    private final Typeface mFont;
    private final Random mRnd;

    public EyeChart(Context context) {
        mContext = context;
        mFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/Eyechart.ttf");
        mRnd = new Random(System.currentTimeMillis());
    }

    @Override
    public Typeface getFont() {
        return mFont;
    }

    @Override
    public char getNextSymbol() {
        //a-d
        char c;

        c = (char) (mRnd.nextInt(4) + 'a');
        return c;
    }
    @Override
    public float getFontScale() {
        return 1.7f;
    }

    @Override
    public float getVerticalShift(int width) {
        return -0.4f * (float)width;
    }
}
