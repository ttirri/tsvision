package com.example.tsvision;

import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.view.View;
import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.util.AttributeSet;

import android.os.Build;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class GlyphView extends View implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final PorterDuffXfermode mXfermode;
    private final Paint mBitmapPaint;
    private int mWidth;
    private int mHeight;

    private float basicGlyphHeight = 2.916667f; // in mm, for 1.0 vision
    private float visionStrengths[];
    
    private float mInclination;
    private int mScreenHeightPixels;
    private float mYppi;
    private float mYppmm;
    private float mDensity;

    private Bitmap mStaticBitmap;
    private Canvas mStaticCanvas;
    private Bitmap mDynamicBitmap;
    private Canvas mDynamicCanvas;

    private Context mContext;
    private VisionChart mVisionChart;
    private SharedPreferences mPrefs;
    private Typeface mInstrumentFont;
    private int mCurrentVisionIndex;
    private TextView mTextView;

    public GlyphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        mXfermode = new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER);
        mBitmapPaint = new Paint();
        mBitmapPaint.setFilterBitmap(false);

        visionStrengths = new float [ ] { 0.1f, 0.2f, 0.333f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f, 1.25f, 1.5f };

        DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
        mScreenHeightPixels = dm.heightPixels;
        mYppi = dm.ydpi;
        mYppmm = mYppi / 25.45f;
        mDensity = dm.density;

        mCurrentVisionIndex = 0;
        mInstrumentFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/D-DIN.ttf");

        mPrefs = context.getSharedPreferences("com.example.tsvision_preferences", Context.MODE_PRIVATE);
        mPrefs.registerOnSharedPreferenceChangeListener(this);
        String value = mPrefs.getString(SettingsFragment.KEY_VISIONCHART_PREFERENCE, null);

        setChart(value);
    }

    private void setChart(String value) {
        if ("2".equals(value)) {
            mVisionChart = new EyeChart(mContext);
        } else if ("1".equals(value)) {
            mVisionChart = new OptikerChart(mContext);
        } else {
            mVisionChart = new SnellenChart(mContext);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SettingsFragment.KEY_VISIONCHART_PREFERENCE)) {
            String value = sharedPreferences.getString(key, null);
            setChart(value);
            drawDynamic(mDynamicCanvas);
            invalidate();
        }
    }

    public void passTextView(TextView view) {
        mTextView = view;
    }

    private float getMultiplier() {
        return 1.0f / visionStrengths[mCurrentVisionIndex];
    }

    private void drawVisionChartSymbol(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(1.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setTypeface(mVisionChart.getFont());

        float size = mYppmm * getMultiplier() * mVisionChart.getFontScale() * basicGlyphHeight;
        p.setTextSize(size);

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        float width = canvas.getWidth();
        float height = canvas.getHeight();

        String text = String.valueOf(mVisionChart.getNextSymbol());

        float x = width / 2.0f;
        float y = height / 2.0f;

        //draw angle text
        Rect textRect = new Rect();
        p.getTextBounds(text, 0, text.length(), textRect);
        x = x - textRect.width() / 2.0f + mVisionChart.getVerticalShift(textRect.width());
        y = y + textRect.height() / 2.0f;

        canvas.drawText(text, x, y, p);
    }

    private void drawVisionStrenght(Canvas canvas) {
        Paint p = new Paint();
        p.setAntiAlias(true);
        p.setColor(Color.BLACK);
        p.setStrokeWidth(2.0f);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setTypeface(mInstrumentFont);
        p.setTextSize(40.f);

        float width = canvas.getWidth();
        float height = canvas.getHeight();

        String text = "Acuity: " + String.valueOf(visionStrengths[mCurrentVisionIndex]);

        float x = width / 2.0f;
        float y = 5 * height / 6.0f;

        //draw angle text
        Rect textRect = new Rect();
        p.getTextBounds(text, 0, text.length(), textRect);
        x = x - textRect.width() / 2.0f;
        y = y + textRect.height() / 2.0f;

        canvas.drawText(text, x, y, p);
    }

    private void drawDynamic(Canvas canvas) {
        drawVisionChartSymbol(canvas);
        //drawVisionStrenght(canvas);
        updateTextView();
    }

    private void updateTextView() {
        if (mTextView != null) {
            String value = mContext.getString(R.string.acuity);
            value += ":" + String.valueOf(visionStrengths[mCurrentVisionIndex]);
            mTextView.setText(value);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = w;
        mHeight = h;

        if (mWidth != 0 && mHeight != 0 && mStaticBitmap == null) {
            mStaticBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mStaticCanvas = new Canvas(mStaticBitmap);
            int bkColor = Color.parseColor(mContext.getString(R.color.acuity_background));
            mStaticCanvas.drawColor(bkColor);
            //,PorterDuff.Mode.CLEAR
        }

        if (mWidth != 0 && mHeight != 0 && mDynamicBitmap == null) {
            mDynamicBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mDynamicCanvas = new Canvas(mDynamicBitmap);
            mDynamicCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            drawDynamic(mDynamicCanvas);
        }
    }

    private int saveLayer(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return canvas.saveLayer(0, 0, mWidth, mHeight, null);
        } else {
            //noinspection deprecation
            return canvas.saveLayer(0, 0, mWidth, mHeight, null, Canvas.ALL_SAVE_FLAG);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {

        int sc = saveLayer(canvas);

        canvas.drawBitmap(mStaticBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(mXfermode);

        canvas.drawBitmap(mDynamicBitmap, 0, 0, mBitmapPaint);
        mBitmapPaint.setXfermode(null);

        canvas.restoreToCount(sc);
    }

    public void nextRight() {
        drawDynamic(mDynamicCanvas);
        invalidate();
    }

    public void nextLeft() {
        drawDynamic(mDynamicCanvas);
        invalidate();
    }

    public void nextUp() {
        mCurrentVisionIndex++;
        if (mCurrentVisionIndex >= visionStrengths.length)
            mCurrentVisionIndex = visionStrengths.length - 1;

        drawDynamic(mDynamicCanvas);
        invalidate();
    }

    public void nextDown() {
        mCurrentVisionIndex--;
        if (mCurrentVisionIndex < 0)
            mCurrentVisionIndex = 0;

        drawDynamic(mDynamicCanvas);
        invalidate();
    }
}


