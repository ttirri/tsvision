package com.example.tsvision;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Random;

public class OptikerChart implements VisionChart {
    private final Typeface mFont;
    private final Context mContext;
    private final Random mRnd;

    public OptikerChart(Context context) {
        mContext = context;
        mFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/Optiker-K.ttf");
        mRnd = new Random(System.currentTimeMillis());
    }

    @Override
    public Typeface getFont() {
        return mFont;
    }

    @Override
    public char getNextSymbol() {
        //A-Z, 1-9
        char c;

        if (mRnd.nextBoolean())
            c = (char) (mRnd.nextInt(25) + 'A');
        else
            c = (char) (mRnd.nextInt(8) + '1');

        return c;
    }
    @Override
    public float getFontScale() {
        return 2.0f;
    }

    @Override
    public float getVerticalShift(int width) {
        return 0.0f;
    }
}
