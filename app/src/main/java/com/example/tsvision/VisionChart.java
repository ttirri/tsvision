package com.example.tsvision;

import android.graphics.Typeface;

public interface VisionChart {
    Typeface getFont();
    float getFontScale();
    char getNextSymbol();
    float getVerticalShift(int width);
}
